
function checkValid(id, minval, maxval) {
    var target=$("#"+id);
    if(target.val() == null || target.val().length==0 || /^\s+$/.test(target.val()) || isNaN(target.val()) ||  target.val()<minval || target.val()>maxval){
        target.addClass("is-invalid");
        target.parent().children("label").addClass("text-danger");
        return false;
    }else {
        target.removeClass("is-invalid");
        target.parent().children("label").removeClass("text-danger");
        return true;
    }
}


var progress=d3.select(".progress").select(".progress-bar");

$("button").click(function(){
    var allValid=true;
    if(!checkValid("submitInput",0,5000)){
        allValid=false
    }
    /*
    if(!checkValid("inputLineDistance",0,3)){
        allValid=false
    }
    */
    if(!checkValid("inputMassDefect",0,0.1)){
        allValid=false
    }

    if(allValid){
        var database=$("#selectDatabase").val();
        var Form={
            mass: $("#submitInput").val(),
            lineDistance: 0.01,//$("#inputLineDistance").val()
            massDefect: $("#inputMassDefect").val(),
            database: database
        };

        return restCall(Form,database);
    }

});

function restCall(Form,database) {
    $.ajax({
        type: "POST",
        url: "http://localhost:8080/kendrick-backend/webapi/formula",
        data: JSON.stringify(Form),
        contentType: 'text/plain',

        beforeSend: function () {
            // Show image container

            d3.select(".progress").style("visibility", "visible");
            progress.transition()
                .style("width", 20 + "%")
                .attr("aria-valuenow", 20);

        },
        complete: function () {
            progress.transition()
                .style("width", 100 + "%")
                .attr("aria-valuenow", 100);

            d3.select(".progress").transition().delay(1000).duration(1000).style("visibility", "hidden")
                .select(".progress-bar")
                .style("width", 0 + "%")
                .attr("aria-valuenow", 0);


        },
        dataType: "json",
        success: function (results) {
            var uri = "http://localhost:8080/kendrick-backend/webapi/points/" + database;

            $.getJSON(uri, function (data) {

                showScatterPlot(data, results);

            });

        }
    });
}



d3.select("body").append("svg")
    .attr("height", 1)
    .attr("width", 1);

var table = d3.select("#table-location")
        .append("table")
        .attr("class", "table table-condensed"),
    thead = table.append("thead"),
    tbody = table.append("tbody");
// Get every column value

var headerRow={
    formula: "Formula",
    massdefect: "Mass difference",
    //linedistance: "Line Distance"
};
var columns = Object.keys(headerRow);

var c=["Formula", "Mass difference"];//"Line Distance"

thead.append("tr")
   // .attr("class","info")
    .selectAll("th")
    .data(c)
    .enter()
    .append("th")
    .text(function(d){ return d;});

var margin = { top: 20, right: 20, bottom: 30, left: 30 };
width = 550 - margin.left - margin.right,
    height = 450 - margin.top - margin.bottom;

var tooltip = d3.select("body").append("div")
    .attr("class", "tooltip")
    .style("opacity", 0);

var x = d3.scaleLinear()
    .range([0, width])
    .nice();

var y = d3.scaleLinear()
    .range([height, 0]);

var xAxis = d3.axisBottom(x).ticks(12),
    yAxis = d3.axisLeft(y).ticks(12 * height / width);

var svg = d3.select("#scatter-load").append("svg")
    .attr("width", width + margin.left + margin.right)
    .attr("height", height + margin.top + margin.bottom)
    .append("g")
    .attr("transform", "translate(" + margin.left + "," + margin.top + ")");

var clip = svg.append("defs").append("svg:clipPath")
    .attr("id", "clip")
    .append("svg:rect")
    .attr("width", width )
    .attr("height", height )
    .attr("x", 0)
    .attr("y", 0);

// setup fill color
var color = d3.scaleOrdinal(d3.schemeCategory10)
    .domain(["Norine", "PubChem", "All", "Unknown"]);
/*
var color = d3.scaleThreshold()
    .domain(["Norine", "PubChem", "All", "Unknown"])
    .range(["blue", "green", "white","red"]);
*/
var scatter = svg.append("g")
    .attr("id", "scatterplot")
    .attr("clip-path", "url(#clip)");

scatter.append("g")
    .attr("class", "brush");

svg.append("text")
    .style("text-anchor", "end")
    .attr("x", width)
    .attr("y", height - 8)
    .text("Nominal Kendrick Mass");


svg.append("text")
    .attr("transform", "rotate(-90)")
    .attr("y", 6)
    .attr("dy", "1em")
    .style("text-anchor", "end")
    .text("Regular Kendrick Mass Defect [CH2]");

// x axis
svg.append("g")
    .attr("class", "x axis")
    .attr('id', "axis--x")
    .attr("transform", "translate(0," + height + ")")
   .call(xAxis);

// y axis
svg.append("g")
    .attr("class", "y axis")
    .attr('id', "axis--y")
    .call(yAxis);

var legend = svg.selectAll(".legend")
    .data(color.domain())
    .enter().append("g")
    .attr("class", "legend")
    .attr("transform", function(d, i) { return "translate(0," + i * 20 + ")"; });

legend.append("rect")
    .attr("x", width - 18)
    .attr("width", 18)
    .attr("height", 18)
    .style("fill", color);

legend.append("text")
    .attr("x", width - 24)
    .attr("y", 9)
    .attr("dy", ".35em")
    .style("text-anchor", "end")
    .text(function(d) { return d; });

var tooltip = d3.select("body").append("div")
    .attr("class", "tooltip")
    .style("opacity", 0);



function showScatterPlot(data,results) {


    tbody.selectAll("tr").remove();
    scatter.selectAll(".dot").remove();
    scatter.selectAll(".regression").remove();
    d3.select(".alert-warning").style("visibility","hidden");
    if(results.length>0 ){

        d3.select("thead tr").style("visibility","visible");
        var tabledata = [];
        var linesplot = [];

        var unknownPoint=results[0].unknownPoint;

        data.push(unknownPoint);

        data.forEach(function(d) {
            d.y = +d.y;
            d.x = +d.x;

            var formula="";
            if(d.composition!=null){
                var ch="";
                var restAtoms="";
                $.each(d.composition.formulaArray,function (i,atomComp) {
                    var atom=atomComp.atom;
                    if(atom=="C" || atom=="H"){
                        ch=ch+atom;
                        if(atomComp.count>1)
                            ch=ch+"<sub>"+atomComp.count+"</sub>";

                    }else{
                        restAtoms=restAtoms+atom;
                        if(atomComp.count>1)
                            restAtoms=restAtoms+"<sub>"+atomComp.count+"</sub>";

                    }
                    //formula=formula+atomComp.atom;
                    //formula=formula+"<sub>"+atomComp.count+"</sub>";
                });
                formula=ch+restAtoms;
            }else{
                formula="Unknown";
            }

            d.formula=formula;
        });


        $.each(results, function (d,v) {

            var ch="";
            var restAtoms="";
            $.each(v.composition.formulaArray,function (i,atomComp) {

                var atom=atomComp.atom;
                if(atom=="C" || atom=="H"){
                    ch=ch+atom;
                    ch=ch+"<sub>"+atomComp.count+"</sub>";
                }else{
                    restAtoms=restAtoms+atom;
                    restAtoms=restAtoms+"<sub>"+atomComp.count+"</sub>";
                }

            });

            var row={
                formula: ch+restAtoms,
                massdefect: v.massDefect.toFixed(8),
                //linedistance: v.lineDistance.toFixed(8),
                id: d
            };
            tabledata.push(row);
            $.each(v.patternLine, function (i,p) {
                var lg =calcLinePoints(p.line,p.point.x,p.secondPoint.x,d);
                //p.point.id=d;

                var ch="";
                var restAtoms="";
                $.each(p.point.composition.formulaArray,function (i,atomComp) {
                    var atom=atomComp.atom;
                    if(atom=="C" || atom=="H"){
                        ch=ch+atom;
                        if(atomComp.count>1)
                            ch=ch+"<sub>"+atomComp.count+"</sub>";
                    }else{
                        restAtoms=restAtoms+atom;
                        if(atomComp.count>1)
                            restAtoms=restAtoms+"<sub>"+atomComp.count+"</sub>";
                    }

                });
                p.point.formula=ch+restAtoms;

                data.push(p.point);
                linesplot.push(lg);
            });



        });


        var rows = tbody.selectAll("tr")
            .data(tabledata)
            .enter()
            .append("tr")
            .on("mouseover", function(d){
                var idTable=d.id;
                scatter.selectAll("line")
                    .style("stroke",function (d){
                        if(d.id==idTable){

                            return "red";
                        }
                    } );
                /*
                scatter.selectAll(".dot")
                    .style("fill",function (d){
                    if(d.id==idTable){

                        return "red";
                    }
                } );
                */
                d3.select(this)
                    .style("background-color", "#1da71926");


            })
            .on("mouseout", function(d){
                var idTable=d.id;
                scatter.selectAll("line")
                    .style("stroke",function (d){
                        if(d.id==idTable){
                            return "black";
                        }
                    } );

                /*
                            scatter.selectAll(".dot")
                                .style("fill",function (d){
                                    if(d.id==idTable){

                                        return color(d.database);
                                    }
                                } );
                                */

                d3.select(this)
                    .style("background-color","white");


            });


        rows.selectAll("td")
            .data(function(row){
                return columns.map(function(d, i){
                    return {i: d, value: row[d]};
                });
            })
            .enter()
            .append("td")
            .html(function(d){ return d.value;});



        d3.extent(data, function (d) { return d.x; });
        d3.extent(data, function (d) { return d.y; });
        x.domain(d3.extent(data, function (d) { return d.x; })).nice();
        y.domain(d3.extent(data, function (d) { return d.y; })).nice();

        scatter.selectAll(".dot")
            .data(data)
            .enter().append("circle")
            .attr("class", "dot")
            .attr("r", function(d) { return (d.target ? 2.5 : 2); })
            .attr("cx", function (d) { return x(d.x); })
            .attr("cy", function (d) { return y(d.y); })
            .attr("opacity",function(d) { return (d.target ? 1 : 0.8); } )
            .style("fill", function(d) { return color(d.database);})
            .style("stroke",function(d) {
                if (d.target) {
                    return color(d.database);
                }
            })
            .on("mouseover", function(d) {
                tooltip.transition()
                    .duration(200)
                    .style("opacity", .9);
                tooltip.html(d.formula)
                    .style("left", (d3.event.pageX + 5) + "px")
                    .style("top", (d3.event.pageY - 28) + "px");
            })
            .on("mouseout", function(d) {
                tooltip.transition()
                    .duration(500)
                    .style("opacity", 0);
            });

        //.attr("id",function(d) { return d.id });




        scatter.selectAll(".regression").data(linesplot)
            .enter()
            .append("line")
            .attr("class", "regression")
            .attr("id",function(d) { return d.id })
            .attr("x1", function(d) { return x(d.line.x1); })
            .attr("y1", function(d) { return y(d.line.y1); })
            .attr("x2", function(d) { return x(d.line.x2); })
            .attr("y2", function(d) { return y(d.line.y2); });

        svg.select("#axis--x").call(xAxis);
        svg.select("#axis--y").call(yAxis);


        var brush = d3.brush().extent([[0, 0], [width, height]]).on("end", brushended),
            idleTimeout,
            idleDelay = 350;

        scatter.select(".brush").call(brush);

        //d3.select(".progress").select(".progress-bar").transition().delay(650).style("visibility","hidden");
        /*
            d3.select(".progress").select(".progress-bar")
                .style("width",0+"%")
                .attr("aria-valuenow", 0);

        */
    }else{
        d3.select("thead tr").style("visibility","hidden");
        d3.select(".alert-warning").style("visibility","visible");

    }


    function brushended() {

        var s = d3.event.selection;
        if (!s) {
            if (!idleTimeout) return idleTimeout = setTimeout(idled, idleDelay);
            x.domain(d3.extent(data, function (d) { return d.x; })).nice();
            y.domain(d3.extent(data, function (d) { return d.y; })).nice();
        } else {

            x.domain([s[0][0], s[1][0]].map(x.invert, x));
            y.domain([s[1][1], s[0][1]].map(y.invert, y));
            scatter.select(".brush").call(brush.move, null);
        }
        zoom();
    }

    function idled() {
        idleTimeout = null;
    }

    function zoom() {

        var t = scatter.transition().duration(750);
        svg.select("#axis--x").transition(t).call(xAxis);
        svg.select("#axis--y").transition(t).call(yAxis);
        scatter.selectAll("circle").transition(t)
            .attr("cx", function (d) { return x(d.x); })
            .attr("cy", function (d) { return y(d.y); });
        scatter.selectAll("line").transition(t)
            .attr("x1", function(d) { return x(d.line.x1); })
            .attr("y1", function(d) { return y(d.line.y1); })
            .attr("x2", function(d) { return x(d.line.x2); })
            .attr("y2", function(d) { return y(d.line.y2); });

    }
}

function calcLinePoints(line, knownpoint,unknownpoint,id){

    if(unknownpoint>knownpoint){
        maxX=unknownpoint+5;
        minX=knownpoint-5;
    }else{
        maxX=knownpoint+5;
        minX=unknownpoint-5;
    }
    return {
        line :{
            x1: minX,
            y1: line.slope * minX + line.n,
            x2: maxX,
            y2:line.slope * maxX + line.n
        },
        id: id,
        point:knownpoint
    }
}
