package org.expasy.kendrick;

public class Form {
    private double mass;
    private double lineDistance;
    private double massDefect;
    private String database;

    public Form(){

    }
    public Form(double mass) {
        this.mass = mass;
    }

    public double getMass() {
        return mass;
    }

    public double getLineDistance() {
        return lineDistance;
    }

    public double getMassDefect() {
        return massDefect;
    }

    public String getDatabase() {
        return database;
    }
}
