package org.expasy.kendrick;

import org.expasy.mzjava.core.mol.AtomicSymbol;

public class AtomComp {
   AtomicSymbol atomSymbol;
   int count;

    public AtomComp() {
    }

    public AtomComp(AtomicSymbol atomSymbol, int count) {
        this.atomSymbol = atomSymbol;
        this.count = count;
    }

    public AtomicSymbol getAtomSymbol() {
        return atomSymbol;
    }

    public int getCount() {
        return count;
    }
}
