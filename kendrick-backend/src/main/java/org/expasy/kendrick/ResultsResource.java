package org.expasy.kendrick;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.collect.Lists;
import io.CustomObjectMapper;
import io.Reader;
import kendrik.Database;
import kendrik.FormulaPredictor;
import kendrik.SimpleMatch;
import org.expasy.mzjava.core.mol.Composition;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.xml.crypto.Data;
import java.io.IOException;
import java.util.Collection;
import java.util.List;
import java.util.Set;


@Path("/formula")
public class ResultsResource {

    @POST
    //@Consumes("text/plain")
    @Consumes("text/plain")
    @Produces("application/json")

    public String uploadFile(String inputForm) throws IOException {
        CustomObjectMapper objectMapper = new CustomObjectMapper();
        Form form = objectMapper.readValue(inputForm, Form.class);
        //double unknownMass = Double.parseDouble(inputMass);
        double unknownMass = form.getMass();
        Database database=Database.valueOf(form.getDatabase());
        Reader reader= new Reader();
        FormulaPredictor formulaPredictor= new FormulaPredictor(form.getLineDistance(),form.getMassDefect(),reader.readLines(database));
        List<SimpleMatch> formulaList= formulaPredictor.findFormula(unknownMass);

        if(formulaList.isEmpty()) {
            formulaList = formulaPredictor.findFormula2Path(unknownMass);
        }

        return objectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(formulaList);

    }
}
