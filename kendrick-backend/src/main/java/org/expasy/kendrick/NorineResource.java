package org.expasy.kendrick;

import geometry.Point;
import io.CustomObjectMapper;
import io.Reader;
import kendrik.Database;


import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import java.io.IOException;
import java.util.List;

@Path("/points/Norine")
public class NorineResource {


    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public String uploadPoints() throws IOException {
        CustomObjectMapper objectMapper = new CustomObjectMapper();
        Reader reader= new Reader();
        List<Point> points=reader.readPoints(Database.Norine);
        return objectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(points);


    }

}
